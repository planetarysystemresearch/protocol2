import numpy as np
import sys

avg = None
count = 0

for i in range(1,int(sys.argv[1])+1):
    file0 = "tmp/siris"+str(i)+"/outputS_001.out"
    data = np.loadtxt(file0)
    if avg is None:
        avg = data
    else:
        avg += data
    count += 1

avg = avg/count

np.savetxt("tmp/outputS_avg.out",avg)

