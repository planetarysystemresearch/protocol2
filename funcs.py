import numpy as np

def get_params():
    params = {}
    with open("PARAMS","r") as f:
        lines=f.readlines()
        for line in lines:
            if(len(line)<=1): continue
            param = line.strip().split()
            params[param[0]] = param[1]


    params["SET_K"] = str(2*np.pi/float(params["SET_WAVEL"]))

    return params

def replace_in(lines,params):
    for i in range(0,len(lines)):
        for k, v in params.items(): 
            lines[i] = lines[i].replace(k,v)

def write_to(fname,lines):
    with open(fname,"w") as f:
        for line in lines:
            f.write(line)


def fill(fname,outputf,params_list):
    lines = []
    with open(fname,"r") as f:
        lines = f.readlines() 
    for params in params_list:
        replace_in(lines,params)
    write_to(outputf,lines)

