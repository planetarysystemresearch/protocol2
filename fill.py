import funcs as g
import os

params = g.get_params()


WRKDIR = os.path.dirname(os.path.realpath(__file__))
params["CURRENT_WRK_DIR"] = WRKDIR


eps = (float(params["SET_M_R"])+float(params["SET_M_I"])*1.0j)**2 

params["SET_EPS_R"] = str(eps.real) 
params["SET_EPS_I"] = str(eps.imag)

g.fill("run_mlfmm_proto.sh","tmp/run_mlfmm.sh",[params])
g.fill("run_siris_proto.sh","tmp/run_siris.sh",[params])
g.fill("run_rtcb_proto.sh","tmp/run_rtcb.sh",[params])
g.fill("run_process_mlfmm_proto.sh","tmp/run_process_mlfmm.sh",[params])
g.fill("run_finalize_proto.sh","tmp/run_finalize.sh",[params])

