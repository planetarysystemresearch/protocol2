#!/bin/bash -l

#SBATCH -J combine
#SBATCH -t 00:01:00
#SBATCH -o step_4_o
#SBATCH -e step_4_e
#SBATCH --mem-per-cpu=100
#SBATCH -p serial
#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=SET_EMAIL_HERE


module load python-env/3.5.3
cd CURRENT_WRK_DIR
python avg.py SET_NCORES_HERE
python combine.py

