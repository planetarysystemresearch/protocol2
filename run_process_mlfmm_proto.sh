#!/bin/bash -l

#SBATCH -J process
#SBATCH -t 00:01:00
#SBATCH -o step_2_o
#SBATCH -e step_2_e
#SBATCH --mem-per-cpu=100
#SBATCH -p serial
#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=SET_EMAIL_HERE



module load python-env/3.5.3
cd CURRENT_WRK_DIR
python process_mlfmm.py
cp tmp/siris_pmatrix_mlfmm_00001.in tmp/pmatrix1.in
mv pmdec*.out tmp/
mv smdec0.out tmp/  
mv suv.out tmp/
