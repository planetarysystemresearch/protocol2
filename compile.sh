#!bin/bash
module load gcc openmpi openblas hdf5-serial

git clone git@bitbucket.org:planetarysystemresearch/t-matrix_mlfmm.git mlfmm
cd mlfmm
git checkout copy2
cd src
make
cp t_matrix ../../
cd ../..

git clone git@bitbucket.org:planetarysystemresearch/siris4.git
cd siris4
cd src
make OPTI=2
cp siris4 ../../siris
cd ../..

git clone git@bitbucket.org:planetarysystemresearch/rtcb_public.git rtcb
cd rtcb
mkdir obj
mkdir mod
make sphereMPI
cp rtcbSphere_MPI ../
cd ..

git clone git@bitbucket.org:planetarysystemresearch/m_inversion_tool.git invtool
cd invtool
cd src
gfortran pmdec.f90 -o ../../pmdec
cd ../mietool
gfortran -O3 mie.f90 run.f90 -o ../../mie
cd ../..


mkdir tmp

