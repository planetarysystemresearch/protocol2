#!/bin/bash -l

#SBATCH -J siris
#SBATCH -t SET_SIRIS_TIME
#SBATCH --array=1-SET_NCORES_HERE
#SBATCH -o step_3_b_o
#SBATCH -e step_3_b_e
#SBATCH --mem-per-cpu=SET_SIRIS_MEM
#SBATCH -p serial
#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=SET_EMAIL_HERE

module load gcc openmpi openblas hdf5-serial
sleep ${SLURM_ARRAY_TASK_ID}
cd CURRENT_WRK_DIR/tmp/
mkdir siris${SLURM_ARRAY_TASK_ID} 
cd siris${SLURM_ARRAY_TASK_ID} 
cp ../siris_pmatrix_mlfmm_00001.in ./pmatrix1.in
srun --mpi=pmi2 ../../siris ../run_siris.in

 
