#!/bin/bash -l

#SBATCH -J mlfmm
#SBATCH -t SET_MLFMM_TIME
#SBATCH -e step_1_e
#SBATCH -o step_1_o
#SBATCH --mem-per-cpu=SET_MLFMM_MEM
#SBATCH -p parallel
#SBATCH -n SET_NCORES_HERE
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=SET_EMAIL_HERE

module load gcc openmpi openblas hdf5-serial
cd CURRENT_WRK_DIR/tmp/
srun ../t_matrix -k SET_K -elem_ka SET_ELEM_K -N_ave SET_MLFMM_AVG -density SET_DENSITY -eps_r SET_EPS_R -eps_i SET_EPS_I -min_radius SET_MIN_RADIUS -max_radius SET_MAX_RADIUS -stdev SET_STDEV -mean SET_MEAN

