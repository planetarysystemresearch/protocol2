import numpy as np
import funcs as g
import subprocess


old_params = g.get_params()
new_params = {}


lines = []
with open("tmp/kappa_mlfmm_00001.dat","r") as f:
    lines = f.readlines()

def fill(new_params,line,name_in_mlfmm,new_name):
    if name_in_mlfmm in line:
        params=line.strip().split();
        new_params[new_name] = params[2]


for i in range(0,len(lines)):
    fill(new_params,lines[i],"incoherent_mean_free_path","SET_MFP")
    fill(new_params,lines[i],"albedo","SET_SSA")
    fill(new_params,lines[i],"Csca_tot","SET_CSCA")

##MIE TOOL
out = subprocess.Popen(["./mie",new_params["SET_CSCA"],old_params["SET_WAVEL"],old_params["SET_ELEM_K"],new_params["SET_MFP"],"./tmp/output"])
out.wait()

lines = None
with open("./tmp/output","r") as f:
    lines = f.readlines()

new_params["SET_APP_R"] = lines[0].split()[0]
new_params["SET_APP_I"] = lines[0].split()[1]


##invtool
fname = "tmp/mueller_mlfmm_00001.dat"

line_count=0
with open(fname) as f:
    for i, l in enumerate(f):
        line_count=line_count+1
print(str(int(line_count)))

out = subprocess.Popen(["./pmdec",fname,str(int(line_count-1))])
out.wait()

A=np.loadtxt("pmdec0.out")
B=np.loadtxt("smdec0.out")

B = np.delete(B,0,axis=1)
C = np.concatenate((A,B),axis=1)
np.savetxt("tmp/pnew.out",C)





##rtcb
g.fill("run_rtcb_proto.in","tmp/run_rtcb.in",[old_params,new_params])

##siris
g.fill("run_siris_proto.in","tmp/run_siris.in",[old_params,new_params])






