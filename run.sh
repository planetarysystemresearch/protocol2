#!bin/bash

python fill.py
cd tmp
mlfmm=$(sbatch --parsable run_mlfmm.sh)

mlfmmready=$(sbatch --parsable --dependency=afterok:$mlfmm run_process_mlfmm.sh)
#mlfmmready=$(sbatch --parsable run_process_mlfmm.sh)

rtcb=$(sbatch --parsable --dependency=afterok:$mlfmmready run_rtcb.sh)
siris=$(sbatch --parsable --dependency=afterok:$mlfmmready run_siris.sh)

finalize=$(sbatch --parsable --dependency=afterok:$rtcb,$siris run_finalize.sh)
#finalize=$(sbatch --parsable --dependency=afterok:$siris run_finalize.sh)
